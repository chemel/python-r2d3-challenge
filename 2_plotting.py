import csv
import matplotlib.pyplot as plt

plot1 = []
plot2 = []

with open('data.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        # print(row['id'])
        if row['commune'] == 'Clermont-Ferrand':
            plot1.append(float(row['valeur_fonciere']))
        elif row['commune'] == 'Bordeaux':
            plot2.append(float(row['valeur_fonciere']))

# print(plot1)

plot1.sort()
plot2.sort()

plt.plot(plot1, 'o')
plt.plot(plot2, 'o')
plt.xlabel('house')
plt.ylabel('price')
plt.show()
