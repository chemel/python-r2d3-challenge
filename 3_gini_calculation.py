import csv
from gini import gini;

values = []

with open('data.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        values.append(float(row['valeur_fonciere']))

values.sort()

print(values)
print(gini(values))
