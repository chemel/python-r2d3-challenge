# Python R2D3 challenge

Mise en pratique d'un algorithme d'apprentissage automatique en python en suivant l'article de r2d3.us : http://www.r2d3.us/visual-intro-to-machine-learning-part-1/

## Installation requirements

pip3 install matplotlib

## gini.py function source code

https://github.com/sdpython/ensae_teaching_cs/blob/master/src/ensae_teaching_cs/ml/gini.py

## Resources

https://machinelearningmastery.com/implement-decision-tree-algorithm-scratch-python/


