import requests
import csv

urls = [
    'https://www.immo-data.fr/api/transactions/search?bounds=3.0176454981960035,45.808609654923,3.1849763792677663,45.808609654923,3.1849763792677663,45.75402416965167,3.0176454981960035,45.75402416965167&propertyType=1&roomCount=1,2,3,4,5&minDate=2014-01-01&maxDate=2019-06-01&minSellPrice=null&maxSellPrice=null&minSurface=null&maxSurface=null&minSquareMeterPrice=NaN&maxSquareMeterPrice=NaN',
    'https://www.immo-data.fr/api/transactions/search?bounds=-0.768179176019089,44.900443832905665,-0.3901808239660909,44.900443832905665,-0.3901808239660909,44.77506596467228,-0.768179176019089,44.77506596467228&propertyType=1&roomCount=1,2,3,4,5&minDate=2014-01-01&maxDate=2019-06-01&minSellPrice=null&maxSellPrice=null&minSurface=null&maxSurface=null&minSquareMeterPrice=NaN&maxSquareMeterPrice=NaN'
]

def getjson(url):
    r = requests.get(url)
    # print(r.status_code)
    # print(r.json())
    data = r.json()['data']
    return data

with open('data.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow([
        'id',
        'commune',
        'valeur_fonciere',
        'prix_surface',
        'surface_batiment',
        'surface_terrain',
        'nombre_pieces'
    ])
    for url in urls:
        data = getjson(url)
        for good in data:
            # print(good['internal_id'])
            writer.writerow([
                good['internal_id'],
                good['adresse']['nom_commune'],
                good['valeur_fonciere'],
                good['prix_surface'],
                good['surface']['surface_reelle_bati'],
                good['surface']['surface_terrain'],
                good['nombre_pieces_principales']
            ])
